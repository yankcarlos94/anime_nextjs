import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      position: "fixed",
      left: 0,
      bottom: 0,
      width: "100%",
      backgroundColor: "#FFF",
      textAlign: "left",
      paddingLeft: 40
    },
    p: {
      color: "#BDBDBD",
      fontSize: 14
    }
  })
);
export default function Footer() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <p className={classes.p}>© 2020 Animes, Inc.</p>
    </div>
  );
}
