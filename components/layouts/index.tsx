import Header from "./header";
import Footer from "./footer";
export default function Layaout(props) {
  const { children, title } = props;
  return (
    <div>
      <Header title={title} />
      {children}
      <Footer />
      <style jsx>{`
        div {
          overflow-x: hidden;
        }
      `}</style>
    </div>
  );
}
