import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import truncate from "lodash/truncate";
import moment from "moment";
import Link from "next/link";

const useStyles = makeStyles({
  root: {
    maxWidth: 345
  }
});

export default function CardGridList(props) {
  const { anime } = props;
  const classes = useStyles();

  return (
    <Card className={classes.root} elevation={5}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt={
            anime.attributes.titles.en === undefined
              ? anime.attributes.titles.en_jp
              : anime.attributes.titles.en
          }
          //height="50%"
          image={anime.attributes.posterImage.original}
          title={
            anime.attributes.titles.en === undefined
              ? anime.attributes.titles.en_jp
              : anime.attributes.titles.en
          }
        />
        <CardContent>
          <Typography
            gutterBottom
            variant="body2"
            component="h5"
            color="textSecondary"
          >
            Created {moment(anime.attributes.createdAt).fromNow()}
          </Typography>
          <Typography gutterBottom variant="h5" component="h2">
            {anime.attributes.titles.en === undefined
              ? anime.attributes.titles.en_jp
              : anime.attributes.titles.en}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {truncate(anime.attributes.synopsis, {
              separator: "...",
              length: 90
            })}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Link
          href={{
            pathname: "/anime/[id]",
            query: { anime: JSON.stringify(anime) }
          }}
          as={`/anime/${
            anime.attributes.titles.en === undefined
              ? anime.attributes.titles.en_jp
              : anime.attributes.titles.en
          }`}
        >
          <Button size="small" color="primary">
            Read More
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
}
