import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Layouts from "../../../components/layouts";
import Typography from "@material-ui/core/Typography";
import moment from "moment";
import capitalize from "lodash/capitalize";

import {
  DynamicFeedRounded,
  TvRounded,
  FavoriteRounded
} from "@material-ui/icons";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minHeight: "100%",
      overflowY: "hidden",
      paddingTop: 90,
      paddingBottom: 50,
      paddingLeft: 20,
      paddingRight: 20
    },
    cover: {
      width: "100%",
      borderRadius: 5,
      maxHeight: 450
    },
    paperRight: {
      width: "80%"
    },
    paperLeft: {
      padding: theme.spacing(2),
      textAlign: "left",
      color: theme.palette.text.secondary,
      width: "80%"
    },
    txtIcon: {
      marginLeft: 7,
      marginRight: 10
    },
    gridContainerIconWithTxt: {
      paddingTop: 20
    }
  })
);

export default function AboutAnimes({ anime }) {
  const classes = useStyles();
  return (
    <Layouts title="Animes">
      <Grid
        container
        spacing={3}
        direction="row"
        justify="space-between"
        alignItems="center"
        className={classes.root}
      >
        <Grid
          item
          xs={6}
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
        >
          <Paper className={classes.paperRight} elevation={5}>
            <img
              className={classes.cover}
              src={
                anime.attributes.coverImage === null
                  ? anime.attributes.posterImage.small
                  : anime.attributes.coverImage.original
              }
              title={anime.attributes.titles.en_jp}
            />
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paperLeft} elevation={0}>
            <Typography
              gutterBottom
              variant="body2"
              component="h5"
              color="textSecondary"
            >
              Update {moment(anime.attributes.updatedAt).fromNow()}
            </Typography>
            <Typography variant="h4" gutterBottom color="textPrimary">
              {anime.attributes.titles.en === undefined
                ? anime.attributes.titles.en_jp
                : anime.attributes.titles.en}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {anime.attributes.synopsis}
            </Typography>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
              className={classes.gridContainerIconWithTxt}
            >
              <FavoriteRounded color="primary" />
              <Typography
                variant="subtitle2"
                gutterBottom
                className={classes.txtIcon}
              >
                {anime.attributes.favoritesCount}
              </Typography>

              <TvRounded color="primary" />
              <Typography
                variant="subtitle2"
                gutterBottom
                className={classes.txtIcon}
              >
                {capitalize(anime.attributes.status)}
              </Typography>

              <DynamicFeedRounded color="primary" />
              <Typography
                variant="subtitle2"
                gutterBottom
                className={classes.txtIcon}
              >
                Episodes {anime.attributes.episodeCount}
              </Typography>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </Layouts>
  );
}

AboutAnimes.getInitialProps = async ({ query }) => {
  const { anime, id } = query;
  if (anime === undefined) {
    const res = await fetch(`https://kitsu.io/api/edge/anime/${id}`);
    const json = await res.json();
    return { anime: json.data };
  }
  return { anime: JSON.parse(anime) };
};
