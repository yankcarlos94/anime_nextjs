import fetch from "isomorphic-unfetch";
import Layouts from "../components/layouts";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import CardGridList from "../components/cardGridList";
import Link from "next/link";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minHeight: "100%",
      overflowY: "hidden",
      paddingTop: 90,
      paddingBottom: 50,
      paddingLeft: 20,
      paddingRight: 20
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary
    }
  })
);

export default function Home({ animes }) {
  const classes = useStyles();
  return (
    <Layouts title="Animes">
      <Grid container spacing={2} className={classes.root}>
        {animes.data.map((item, key) => (
          <Link
            href={{
              pathname: "/anime/[id]",
              query: { anime: JSON.stringify(item) }
            }}
            as={`/anime/${
              item.attributes.titles.en === undefined
                ? item.attributes.titles.en_jp
                : item.attributes.titles.en
            }`}
          >
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
              item
              sm={12}
              md={6}
              lg={3}
              xl={3}
              xs={4}
              key={key}
            >
              <CardGridList anime={item} />
            </Grid>
          </Link>
        ))}
      </Grid>
    </Layouts>
  );
}

Home.getInitialProps = async ctx => {
  const res = await fetch("https://kitsu.io/api/edge/anime?page[limit]=20");
  const json = await res.json();
  return { animes: json };
};
